import React from 'react';
import './formToDo.css'
class FormToDo extends React.Component{
    
    constructor(){
        super();
        this.state = {
            text: ""
        }   
    }

    handleChange =(event) =>{
        this.setState( { text: event.target.value } )
    }
    handleSubmit = (event) => {
        event.preventDefault(); 
            if (this.state.text.trim() ==""){}
            else {
                this.props.AddToDo({
                    text: this.state.text,
                    complete: false,
                    selected: false
                })
                this.setState({
                    text:''
                })
            }
    }

    render(){
        return (
            <>
            <form onSubmit={(e) => this.handleSubmit(e)}>
                <input 
                type="text" 
                value = {this.state.text}
                onChange={ (e) => this.handleChange(e)}
                />
                <br></br>
                <button>Adicionar</button>
                
            </form>
            </>
        )
    }

}
export default FormToDo

