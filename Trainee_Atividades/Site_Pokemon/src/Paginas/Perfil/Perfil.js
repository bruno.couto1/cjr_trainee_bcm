import React from 'react';
import axios from 'axios';
import { navigate } from "@reach/router";
import { myContext } from '../../App';
import './perfil.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';


//////RETIRAR ADICIONA

function Perfil (){
    const {value, setValue } = React.useContext(myContext);
    const [userData, setUserData] = React.useState();
    const [flag, setFlag] = React.useState();
    const [poke, setPoke] = React.useState();

    React.useEffect ( () => {
        //carrega os dados do usuário e coloca em userData
        axios.get( (" https://pokedex20201.herokuapp.com/users/" + value.user))
        .then((e) => {
            console.log(e.data)
            setUserData(e.data); 
        })
        return;
    },[flag])
    
    /*React.useEffect ( () => {//auxiliar
        //carrega os dados do pokemon e coloca em poke
        axios.get( (" https://pokedex20201.herokuapp.com/pokemons"))
        .then((e) => {
            console.log(e.data.data)
            setPoke(e.data.data)
        })
        return;
    },[])*/

    /*const adicionaPokemon = async (pokename, username) =>{
        if(userData){
            if(userData.pokemons.some((e) => e.name === pokename)){
                //ignora
                console.log("ERRO: item " + pokename + " já existe na lista de favoritos")
            }
            else {//adiciona
                await axios.post( (" https://pokedex20201.herokuapp.com/users/" + username + '/starred/' + pokename))
                setFlag(!flag);
            }
        }
    }*/

    const retiraPokemon = async (pokename, username) =>{
        if(userData){
            if (userData.pokemons.some((e) => e.name === pokename)){
                //deleta da lista
                await axios.delete( (" https://pokedex20201.herokuapp.com/users/" + username + '/starred/' + pokename))
                setFlag(!flag);
                
            }
            else {
                //ignora
                console.log("ERRO: item " + pokename + " não existe na lista de favoritos")
            }
        }
    }

    const changeToPokemonPage = async (nomePokemon) =>{
        await setValue({
            user: value.user,
            pokemon: nomePokemon
        })
        navigate('dados-pokemon')
    }

    return(
    <>  
        <Header/>
        <div className='perfil-container'>
            <div className='perfil-box'>   
                <h1>Perfil de {userData && userData.user.username}</h1>
                <h4>O id do usuário é {userData && userData.user.id}</h4>
                <h4>O usuário possui {userData && userData.pokemons.length } pokemons favoritos</h4>
            </div>
            <div>
                {userData && userData.pokemons.map( (data) => (
                    <>
                    <div className='pokemon-box'>
                        <h3>{data.name}</h3>
                        <img 
                            src={data.image_url} 
                            alt="imagem do pokemon" 
                            onClick={(e) => changeToPokemonPage(data.name)} />
                        <br/>
                        {/*<button onClick = { (e) => adicionaPokemon( data.name , value.user) }>Adiciona</button>*/}
                        <button className='botao' onClick = { (e) => retiraPokemon( data.name , value.user) }>Desfavoritar</button> 
                    </div>
                    </>
                ))
                }
            </div>
                
        </div>
    </>
    )
};
export default Perfil;
