import React from 'react';
import axios from 'axios';
import { myContext } from '../../App';
import { navigate } from '@reach/router';
import Header from '../Header/Header';
import './Dados_pokemon.css';

function Dados_pokemon_test (){
    const {value, setValue } = React.useContext(myContext);
    const [poke, setPoke] = React.useState();
    const [userData, setUserData] = React.useState();
    const [flag, setFlag] = React.useState();

    React.useEffect ( () => {//carrega os dados do usuário e coloca em userData
        axios.get( (" https://pokedex20201.herokuapp.com/users/" + value.user))
        .then((e) => {
            console.log(e.data)
            setUserData(e.data); 
        })
        return;
    },[flag])

    React.useEffect ( () => {//carrega os dados do pokemon e coloca em poke
        axios.get( (" https://pokedex20201.herokuapp.com/pokemons/" + value.pokemon))
        .then((e) => {
            console.log(e)
            setPoke(e)
        })
        return;
    },[])

    const adicionaPokemon = async (pokename, username) =>{
        if(userData){
            if(userData.pokemons.some((e) => e.name === pokename)){
                //ignora
                console.log("ERRO: item " + pokename + " já existe na lista de favoritos")
            }
            else {//adiciona
                await axios.post( (" https://pokedex20201.herokuapp.com/users/" + username + '/starred/' + pokename))
                setFlag(!flag);
            }
        }
    }

    const retiraPokemon = async (pokename, username) =>{
        if(userData){
            if (userData.pokemons.some((e) => e.name === pokename)){
                //deleta da lista
                await axios.delete( (" https://pokedex20201.herokuapp.com/users/" + username + '/starred/' + pokename))
                setFlag(!flag);
                
            }
            else {
                //ignora
                console.log("ERRO: item " + pokename + " não existe na lista de favoritos")
            }
        }
    }

    const mostraBotãoFavorito = ( pokename , username ) => {
        if(userData){
            if( (userData.pokemons.some((e) => (e.name == pokename)) )  ){
                return (
                    <button className='botao' onClick = { (e) => retiraPokemon( value.pokemon , value.user) }>Desfavoritar</button> 
                )
            }
            else{
                return (
                    <button className='botao' onClick = { (e) => adicionaPokemon( value.pokemon , value.user) }>Favoritar</button>
                )
            }
        }   
    }

    return(
    <>
        <Header/>
        <div className='container-dados-pokemon'>
        <h1 className='Nome'>{value.pokemon}</h1>
        

        <div className= 'imagempoke'> <img 
            src={poke && poke.data.image_url}
            alt="imagem do pokemon" /> </div>
        <ul className='features'>
            <li >id: {poke && poke.data.id}</li>
            
            <li >height: {poke && poke.data.height}</li>
            <li>weight: {poke && poke.data.weight}</li>
            <li>tipo: {poke && poke.data.kind
                .split(';')
                .map((e)=>(
                    <span> {e} </span>
                ))}</li>
        </ul>
        { mostraBotãoFavorito(value.pokemon , value.user) }
        <button className='botao' onClick ={()=>navigate("perfil")}>Ir para Perfil</button>
        </div>
    </>
    )
};
export default Dados_pokemon_test;
