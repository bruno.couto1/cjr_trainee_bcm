import React from 'react';
import FormToDo from './formToDo';
import './listToDo.css';

class ListToDo extends React.Component{
    
    constructor(){
        super();
        this.state = {
            all_ToDos: []
        };
    }
    AddToDo = async (new_todo) => {
        await this.setState({
            all_ToDos: [new_todo , ...this.state.all_ToDos]
        })
        localStorage.setItem("ToDo" , JSON.stringify(this.state.all_ToDos))
    }

    DeleteCompletedToDo = async () => {
        await this.setState({
            all_ToDos:  this.state.all_ToDos.filter(e => !e.complete)
        })
        localStorage.setItem("ToDo" , JSON.stringify(this.state.all_ToDos))

    }
    DeleteSelectedToDo = async () => {
        await this.setState({
            all_ToDos:  this.state.all_ToDos.filter(e => !e.selected)
        })
        localStorage.setItem("ToDo" , JSON.stringify(this.state.all_ToDos))
    }

    ChangeCheckBox = async (e) => {
        {/*Aqui eu mudo apenas o elemento selected do To Do*/}
        await this.setState({
            all_ToDos: this.state.all_ToDos.map( todo =>{
                if (todo.text === e.text){
                    return {
                        text: e.text,
                        complete: todo.complete,
                        selected: !todo.selected
                    };
                }
                else {
                    return todo;
                }
            })
        })
        localStorage.setItem("ToDo" , JSON.stringify(this.state.all_ToDos))
    }
    ChangeComplete = async (text) => {
        {/*Aqui eu mudo apenas o elemento complete do To Do*/}
        await this.setState({
            all_ToDos: this.state.all_ToDos.map( todo =>{
                if (todo.text === text){
                    return {
                        text: text,
                        complete: !todo.complete,
                        selected: todo.selected
                    };
                }
                else {
                    return todo;
                }
            })
        })
        localStorage.setItem("ToDo" , JSON.stringify(this.state.all_ToDos))
    };
    async componentDidMount () {
        {/*Aqui eu mantenho o texto e se está completo, mas coloco todos como não selecionados, pq
        o cliente acbou de abrir a página. */}
        const flag = localStorage.getItem("ToDo")
        if(flag){
            let minha_var = JSON.parse(localStorage.getItem("ToDo"))
            await this.setState({
                all_ToDos: (minha_var.map(todo =>{
                    return{
                        text: todo.text,
                        complete: todo.complete,
                        selected: false
                    }
                }))
            })
            localStorage.setItem("ToDo" , JSON.stringify(this.state.all_ToDos))
        }
        else return;
    }
    render(){
        return(
            <div className='List_Form_container'>
                <h2>Adicionar nova Tarefa</h2>
                <ul>
                    <li>Todos: {this.state.all_ToDos.length}</li>
                    <li>Ativos: {this.state.all_ToDos.filter((e) => !e.complete).length}</li>
                    <li>Completos: {this.state.all_ToDos.filter((e) => e.complete).length}</li>
                </ul>
                <FormToDo AddToDo = {this.AddToDo}/>    
                <div>{
                        this.state.all_ToDos.map((e) =>(
                        <div className='containerEachToDo'>
                            <input type="checkbox" checked={e.selected} onClick = { (algo) => this.ChangeCheckBox(e,algo) }/>
                            <div 
                                className ={ 'eachToDo' + (e.complete ? ' ToDo_complete' : '' )} 
                                onClick={() => this.ChangeComplete(e.text) }
                                key ={e.text} 
                            >
                                {e.text} 
                            </div>
                        </div>
                        ))}
                    <button onClick={this.DeleteCompletedToDo}> Deletar os concluídos</button>
                    <button onClick={this.DeleteSelectedToDo}> Deletar os selecionados</button>

                </div>
                    
            </div> 
        );
    }
}
export default ListToDo;