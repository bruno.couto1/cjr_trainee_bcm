import React, { Component } from 'react';
import api from './api';
import { myContext } from '../../App'
import { navigate } from "@reach/router";
import './Home.css';
import Header from '../Header/Header';



class Home extends Component {

  state= {
    pokes: [],
    page: 1,
    pokeinfo: [],
  }


  componentDidMount(){
    this.loadProducts();
  }

  loadProducts = async (page=1) => {
    const response= await api.get('/pokemons?page='+page);
    this.setState({pokes : response.data.data, pokeinfo : response.data.next_page, page});
    
  };

  prevPage= () => {
    const {page }= this.state;

    if(page === 1) return;

    const pageNumber= page - 1;
    this.loadProducts(pageNumber);

  };
  
  nextPage= () => {
    const {page, pokeinfo }= this.state;

    if(page === pokeinfo.next_page) return;

    const pageNumber= page + 1;
    this.loadProducts(pageNumber);
  }
  


  render (){

    return (
      <div>
        <Header/>
        <myContext.Consumer>
          {
            dado => {
              return <>
              
                <h2 className='Titulo'>Bem vindo, {dado.value.user}</h2>
                <h2 className='Titulo'>Estes são os pokemons disponíveis:</h2>
              </>
            }
          }
        </myContext.Consumer>
        <div className='pokelista'>
          {this.state.pokes.map(poke =>(
            <div className='containeer'>
              <h2 key={poke.id}>{poke.name}</h2>
          <myContext.Consumer>
            {
              dado => {
                const changeToPokemonPage = async (nomePokemon) =>{
                  await dado.setValue({
                      user: dado.value.user,
                      pokemon: nomePokemon
                  })
                  navigate('dados-pokemon')
              }
                return <img 
                    onClick={(e) => changeToPokemonPage(poke.name) } 
                    src={poke.image_url} 
                    alt='imagem do pokemon' />
              } 
            }
          </myContext.Consumer>
          </div>
          ))}
        </div>


        <div className='actions'>
          <button className='botao' onClick={this.prevPage}>Anterior</button>
          <button className='botao' onClick={this.nextPage}>Próxima</button>
        </div>
      </div>
    );
  }
}

export default Home;
